"""Pytest configuration file."""

from pathlib import Path

import pytest

from lolabeta import LolaBeta

TEST_RESOURCES_PATH = Path(__file__).parent / "resources"


@pytest.fixture
def custom_replacement_data():
    return {
        "hi": "hello",
        "there": "world",
    }


@pytest.fixture
def lola():
    context_prompt_file = TEST_RESOURCES_PATH / "test_context_prompt.txt"
    return LolaBeta("http://localhost:1234/v1", context_prompt_file)
