from lolabeta import alternate


def test_reaction(lola):
    lola.conversation_history.append({"role": "user", "content": "Hello, Lola"})
    assert lola.answer_from_local_llm()


def test_manual_commands(lola):
    result = lola.manual_command("Idle")
    assert result
    if result:
        lola.idle_timer.cancel()  # Toggle back the timer to end the idle_timer thread.
    assert lola.manual_command("Head")
    assert lola.manual_command("Reset")


def test_alternate(custom_replacement_data):
    test_text = "Hi there!"
    assert alternate(test_text, custom_replacement_data) == "hello world!"
