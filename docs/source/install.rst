Installation
============

Python and it's virtual environment
-----------------------------------

1. You need at least `Python <https://www.python.org/downloads/>`_ **3.10 or above**.

2. Download the source code of LolaBeta by clicking **Code** > **zip**, and unzip it somewhere on your computer.

.. image:: _static/images/2024-05-01 16_25_55-Amelien Deshams _ LolaBeta · GitLab — Mozilla Firefox.png

3. Open a command prompt and ensure that **Python** is accessible, and the version of **Python** is correct (must be above **3.10**) by typing the following command:

.. code-block::

   where python

.. image:: _static/images/2024-05-01 16_27_01-Installation — Python documentation — Mozilla Firefox.png

If it's correct, you should see ``C:\Python310\python.exe`` (or above like ``C:\Python311\python.exe``).

4. Navigate to where you unzipped LolaBeta's source code using the command ``cd``, for example:

.. code-block::

    cd /d D:\LolaBeta

5. We need to create a virtual environment for LolaBeta's project. You can create one with the following command:

.. code-block::

    python -m venv .venv

*This will create a* ``.venv`` *folder inside LolaBeta's source code*

6. Activate the virtual environment.

.. code-block::

    .venv\Scripts\activate.bat

.. image:: _static/images/2024-05-01 16_32_11-LM Studio.png

*If you run the command* ``where python`` *again, you should see that the first result is a* ``python.exe`` *inside LolaBeta's source code.*

7. Now that our virtual environment is ready, we can install LolaBeta.

.. code-block::

    pip install .

.. image:: _static/images/2024-05-01 16_33_24-Greenshot.png

If the installation is successful, we can now install the Local Language Model.

LM-Studio
---------

1. Donwload and install `LM-Studio <https://lmstudio.ai/>`_

2. Search for the model "Opus-v0-7b" and, depending on your graphics card capacity, download the latest **Quantization Level** with the mention **FULL GPU OFFLOAD POSSIBLE** (the latest one is recommended).

.. image:: _static/images/2024-05-01 16_34_38-LM Studio.png

.. note::

    You can download a **SOME GPU OFFLOAD POSSIBLE** if you want, but it's not recommended as the model will run very slowly on CPU.

.. note::

    You can download and run another model if you want, but it's recommended to run the model "Opus-v0-7b" for LolaBeta.

3. Go to the "Local Inference Server" tab and load the model.

.. image:: _static/images/2024-05-01 16_37_27-LM Studio.png

4. Change some settings according to these screenshots.

.. image:: _static/images/2024-05-01 16_38_36-LM Studio.png

.. note::

    max **GPU Offload** is recommended, but you can lower the number of layers if needed, although it's not recommended as the model will run very slowly on CPU.

.. image:: _static/images/2024-05-01 16_39_21-LM Studio.png

.. image:: _static/images/2024-05-01 16_40_21-LM Studio.png

.. note::

    You can increase or decrease the length of the context following if your context prompt is longer or shorter, this represents the maximum length of the number of tokens (words) in your conversation history.

5. Start the server

.. image:: _static/images/2024-05-01 16_44_42-LM Studio.png

LolaBeta is now ready to be started, but it's not quite fully configured, please check out the next step!
