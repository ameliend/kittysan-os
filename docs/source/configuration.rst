Configuration
=============

Context prompt
--------------

LolaBeta needs a context prompt, which will be contained in a **txt** file.

You need to explain **who she is**, **where she is**, etc... **the more details you provide, the better it will be**. For example, I tell her the whole story of the Hoppou community from VRChat, about the communities, etc... so that she can know what to respond to traveler questions.

You can also indicate **her personality**, whether she is welcoming or cold towards other players... give her details about the avatar she wears as well.

Save this prompt in a ``.txt`` file somewhere on your computer and keep it for the next steps.

.. image:: _static/images/2024-05-0116_55_13-context_prompt_strange_place.txt-Notepad.png

Generate Configuration for the first time
-----------------------------------------

Open a command prompt and execute LolaBeta's main Python file, using the virtual environment we previously set up, following the context prompt file.

.. code-block::

   path\to\LolaBeta\venv\Scripts\python.exe -m lolabeta -cp path\to\your\context_prompt_file.txt

Exit LolaBeta for now by using ``Ctrl+C`` to shut down the application.

.. image:: _static/images/2024-05-0116_57_22-Greenshot.png

Now LolaBeta has saved a default configuration that we can modify.

Configuration Files
-------------------

Open your file explorer and navigate to ``%APPDATA%\LolaBeta``.

.. image:: _static/images/2024-05-0116_58_36-LolaBeta.png

config.ini
++++++++++

- **repetitive_occurrence**: A number representing the number of times before LolaBeta reinitializes the conversation if she repeats the same phrase; this is very useful if she gets stuck on a loop and can break the conversation. As soon as the number of times a phrase is reached, the model automatically reinitializes the conversation.

- **aws_access_key**: Enter your Amazon AWS access key here.

- **aws_secret_key**: Enter your Amazon AWS secret key here.

- **aws_region**: Enter your `region code <https://docs.aws.amazon.com/general/latest/gr/rande.html>`_ here.

- **system**: The system prompt, it's not recommended to change it.

conversation_starter.yaml
+++++++++++++++++++++++++

This file represents **the beginning of the conversation between LolaBeta and the user**, **providing more context for LolaBeta on how she should respond and behave**. The rest of the conversation is then filled in by the application when it's running.

This document should be a ``.yaml`` file.

You can either use the default ``conversation_starter.yaml`` or provide a custom one with the argument ``-cs``.

.. code-block::

   path\to\LolaBeta\venv\Scripts\python.exe -m lolabeta -cp path\to\your\context_prompt_file.txt -cs path\to\your\custom\conversation_starter.yaml

.. image:: _static/images/2024-05-0117_01_45-LolaBeta.png

idle_texts.yaml
+++++++++++++++

This file contains a list of sentences that LolaBeta will display when in the "Idle" phase

.. image:: _static/images/2024-05-0117_02_22-idle_texts.yaml-LolaBeta-VisualStudioCode.png

user_replacements.yaml
++++++++++++++++++++++

**A word replacement dictionary, in case voice recognition confuses words**, e.g. "Hoppou" doesn't exist in the voice recognition dictionary, it will try to recognize similar words such as "Hopper".
**The first word is the word to be replaced by the second word.**

.. image:: _static/images/2024-05-0117_02_42-user_replacements.yaml-LolaBeta-VisualStudioCode.png

Now that LolaBeta is configured, you can run it. Follow the next step to learn about all the available arguments!

