========================
LolaBeta's Documentation
========================

.. image:: _static/images/logo.png

If you want to know how to use **LolaBeta** for yourself, then this is your guide!

.. note::

    This guide is intended for **developer users** and requires some setup, such as **purchasing** virtual cables or an Amazon AWS account (where requests are paid).

Requirements
============

- A second VRChat account (which will be used for LolaBeta)
- 2 `Virtual Cable <https://shop.vb-audio.com/en/win-apps/12-vb-cable-ab.html?SubmitCurrency=1&id_currency=1#/30-donation_s-p1_i_m_a_fan>`_ A + B
- A powerful enough graphics card (to run the local language model)
- An `Amazon AWS <https://aws.amazon.com/>`_ account (for the Amazon Polly text-to-speech)
- (Optional) A kittysan avatar (because I've set up some animations on the avatar that I trigger with OSC)

.. toctree::
   :caption: 🌟 First steps
   :maxdepth: 3

   install
   configuration

.. toctree::
   :caption: 🕹️ Usage
   :maxdepth: 3

   usage

.. toctree::
   :caption: 📚 API
   :maxdepth: 3

   api
