LolaBeta
========

.. currentmodule:: lolabeta.main

.. autoclass:: LolaBeta
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~LolaBeta.CONFIG
      ~LolaBeta.IDLE_DELAY

   .. rubric:: Methods Summary

   .. autosummary::

      ~LolaBeta.answer_from_local_llm
      ~LolaBeta.detect_repetitive_responses
      ~LolaBeta.idle
      ~LolaBeta.init_conversation_history
      ~LolaBeta.manual_command
      ~LolaBeta.perform_head_movement
      ~LolaBeta.play_random_poi
      ~LolaBeta.randomize_movement
      ~LolaBeta.recognize
      ~LolaBeta.reset_conversation
      ~LolaBeta.reset_head_position
      ~LolaBeta.save_config
      ~LolaBeta.send_osc_parameter
      ~LolaBeta.speech
      ~LolaBeta.start_listener
      ~LolaBeta.toggle_idle

   .. rubric:: Attributes Documentation

   .. autoattribute:: CONFIG
   .. autoattribute:: IDLE_DELAY

   .. rubric:: Methods Documentation

   .. automethod:: answer_from_local_llm
   .. automethod:: detect_repetitive_responses
   .. automethod:: idle
   .. automethod:: init_conversation_history
   .. automethod:: manual_command
   .. automethod:: perform_head_movement
   .. automethod:: play_random_poi
   .. automethod:: randomize_movement
   .. automethod:: recognize
   .. automethod:: reset_conversation
   .. automethod:: reset_head_position
   .. automethod:: save_config
   .. automethod:: send_osc_parameter
   .. automethod:: speech
   .. automethod:: start_listener
   .. automethod:: toggle_idle
