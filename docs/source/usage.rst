Usage
=====

List of available arguments
---------------------------

- **-u [--url]**: The base URL of the Inference Server (default is 'http://localhost:1234/v1'), make sure LM-Studio is setup et the server started

.. image:: _static/images/2024-05-0117_12_50-Configuration—Pythondocumentation—MozillaFirefox.png

.. note::

    In this example, I'm using the address ``192.168.0.244:1234/v1`` where ``192.168.0.244`` is the local IP address of my other computer, if I want to use another computer on my network, so that one will contain the AI and the other I'll use for playing VRChat 😊

- **-cp [--context-prompt]**: Path to the context prompt file. This file should contain the context prompt for LolaBeta, where you can provide details on her behavior or what she needs to learn... The file must be a .txt format

.. image:: _static/images/2024-05-0116_57_22-Greenshot.png

- **-cs [--conversation-stater]**: Path to a custom conversation starter file. This file represents the beginning of the conversation between LolaBeta and the user, providing more context for LolaBeta on how she should respond and behave. The rest of the conversation is then filled in by the application when it is running

.. image:: _static/images/2024-05-0117_01_45-LolaBeta.png

- **-i [--input-device-name]**: Name of the input device you want to use for LolaBeta to listen. If not set, it use your default microphone, but it should be a Virtual Cable that listen to VRChat game

- **-o [--output-device-name]**: Name of the output device you want for LolaBeta to use for text to speech synthesis. If not set, it use your default speakers, but it should be a Virtual Cable that will be used as a microphone input in VRChat game

.. image:: _static/images/2024-05-0117_13_32-Configuration—Pythondocumentation—MozillaFirefox.png

- **--head-movement / --no-head-movement**: Enable or disable head movement when LolaBeta respond to a user (True by dafault)

- **-p [--osc-port]**: Port number for VRChat OSC (default is 9000)

- **--idle / --no-idle**: Create an Idle timer for LolaBeta to display predefined texts in the Chatbox when no activity is detected (True by dafault)

Virtual Cables
--------------

1. After installing your `Virtual Cable <https://shop.vb-audio.com/en/win-apps/12-vb-cable-ab.html?SubmitCurrency=1&id_currency=1#/30-donation_s-p1_i_m_a_fan>`_ A + B, you need to make VRChat audio go trough the ``CABLE-A Input``. Go to you **Volume mixer** settings and set VRChat **Output device** to the ``CABLE-A Input``.

.. image:: _static/images/2024-05-0117_22_07-Settings.png

2. In VRChat, choose the ``CABLE-B Output`` as your microphone source.

.. image:: _static/images/2024-05-0117_22_45-VRChat.png

Make sure to run LolaBeta with the following arguments:

.. code-block::

   path\to\LolaBeta\venv\Scripts\python.exe -m lolabeta -cp path\to\your\context_prompt_file.txt -i "CABLE-A" -o "CABLE-B"

Manual Commands
---------------

You can run manual commands to Lolabeta, you can type directly in the command prompt once the application is running:

- **head**: will reset the head movement of LolaBeta

.. image:: _static/images/2024-05-0117_15_24-Configuration—Pythondocumentation—MozillaFirefox.png

- **reset**: will reset the conversation history

.. image:: _static/images/2024-05-0117_15_39-Configuration—Pythondocumentation—MozillaFirefox.png

- **idle**: toggle idle timer

.. image:: _static/images/2024-05-0117_15_55-Configuration—Pythondocumentation—MozillaFirefox.png

- **test**: test your connection to the local language model

.. image:: _static/images/2024-05-0117_16_40-Configuration—Pythondocumentation—MozillaFirefox.png

Anything else will manually speech something from LolaBeta as it was her talking, so you can make LolaBeta manually talking, you can type anything else and she will speech it for you.

.. image:: _static/images/2024-05-0117_17_12-Configuration—Pythondocumentation—MozillaFirefox.png

.. note::

    If you manually speech something, it will be added to the conversation history as it was LolaBeta's answer
