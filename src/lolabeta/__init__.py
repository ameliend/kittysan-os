"""Top-level package for LolaBeta."""

from lolabeta._version import __version__
from lolabeta.main import LolaBeta, alternate

__author__ = """Amelien Deshams"""
__email__ = "a.deshams+git@slmail.me"
__all__ = [
    "LolaBeta",
    "__version__",
    "alternate",
]
