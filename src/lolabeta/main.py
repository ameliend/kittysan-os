"""Main module."""

import configparser
import hashlib
import os
import random
import re
import shutil
import string
import sys
from collections import Counter
from pathlib import Path
from time import sleep

import boto3
import botocore
import botocore.exceptions
import speech_recognition
import vgamepad as vg
import yaml
from audio_reader_osc import read_audio_file
from botocore.config import Config
from chatbox_osc import write_chatbox
from logger import Logger
from openai import APIConnectionError, OpenAI
from pyaudio_devices import get_devices
from pythonosc.udp_client import SimpleUDPClient
from resettabletimer import ResettableTimer

CURRENT_DIR = Path(__file__).parent
CONFIG_PATH = Path(os.environ["APPDATA"]) / "LolaBeta"
POI_SOUNDS = list((CURRENT_DIR / "resources" / "poi").glob("*.mp3"))

logger = Logger("LolaBeta")


class LolaBeta:
    """Represents an interactive non-player character (NPC) designed for VRChat.

    This class contains attributes and methods to simulate a conversationalist NPC that can listen,
    speak, and respond to players using natural language processing and voice recognition.

    I recommend installing https://vb-audio.com/Cable/ first, to create virtual mic input/output for LolaBeta.

    Note
    ----
    The configuration of LolaBeta can be tweak in your %APPDATA%/LolaBeta.
    Please fill aws_access_key and aws_secret_key with your Amazon AWS credentials.
    You can modify the files conversation_starter.yaml, idle_texts.yaml, and user_replacements.yaml.
    Or you can provide your own conversation starter file using the conversation_starter_file argument.

    Parameters
    ----------
    base_url : str
        The base URL of the Inference Server.
        It could be http://localhost:1234/v1 or the IP address of another computer on your local network.
    context_prompt_file : str
        Path to the context prompt file.
        This file should contain the context prompt for LolaBeta, where you can provide details
        on her behavior or what she needs to learn... The file must be a .txt format.
    conversation_starter_file : str, optional
        Path to a custom conversation starter file, , by default None.
        This file represents the beginning of the conversation between LolaBeta and the user,
        providing more context for LolaBeta on how she should respond and behave.
        The rest of the conversation is then filled in by the application when it is running.
    input_device_name : str, optional
        Name of the input device you want to use for LolaBeta to listen, by default None.
        If None, then the default input device is used.
    output_device_name : str, optional
        Name of the output device you want for LolaBeta to use for text to speech synthesis, by default None.
        If None, then the default output device is used.
    head_movement : bool, optional
        Enable head movement when LolaBeta respond to a user, by default True.
    osc_port : int, optional
        Port number for VRChat OSC, by default 9000.

    Attributes
    ----------
    IDLE_DELAY : int or float
        Time in seconds to wait before triggering a random idle text.
    CONFIG: dict
        The default LolaBeta configuration dictionary.
    openai_client: OpenAI
        An instance of an OpenAI client, it's actually not using ChatGPT, but rather local models.
    osc_client: SimpleUDPClient
        The OSC client for VRChat.
    polly_client: botocore.client.Polly
        Amazon Polly client used for text-to-speech with AWS credentials from environment variables.
    config: ConfigParser
        LolaBeta configuration object.
    gamepad : vg.VX360Gamepad()
        An instance of a VG VX360Gamepad, representing a player's controller.
    idle_timer : ResettableTimer
        ResettableTimer object that triggers a random idle text after expiry.
    inverted_move : bool
        Flag to indicate whether NPC head movement should be inverted on the next move.
    listener: speech_recognition.Recognizer.listen_in_background
        The listener object when the listener thread is running, for voice recognition.
    input_device_index : int
        The input device index from device.get_devices with name matching input_device_name.
    output_device : dict
        The output device dict from device.get_devices with name matching output_device_name.
    conversation_history_base_length : int
        The default conversation history length, used to compare with the current conversation history
        if it needs to be reseted.
    conversation_history : list
        A list of chat messages representing the current conversation between user and NPC.
    move_x: float
        The horizontal movement amount for the next idle state.
    move_y: float
        The vertical movement amount for the next idle state.
    move_delay: float
        Delay between NPC head movements in seconds.
    """

    IDLE_DELAY = 120
    CONFIG = {
        "APP": {
            # How many time the same answer from the language model will force reset the conversation
            "repetitive_occurrence": 2,
        },
        "API": {
            "aws_access_key": "",
            "aws_secret_key": "",
            "aws_region": "us-west-2",
        },
        "PROMPT": {
            # This is the system prompt for the model, I don't recommend changing it
            # You better want to change the context prompt in the "context_prompt.txt"
            # Or provide a custom context prompt path using "python -m lolabeta -cp 'your_path.txt'"
            "system": "Text transcript of a never-ending conversation between the User and LolaBeta.",
        },
    }

    def __init__(
        self,
        base_url: str,
        context_prompt_file: str,
        conversation_starter_file: str | None = None,
        input_device_name: str | None = None,
        output_device_name: str | None = None,
        head_movement: bool = True,
        osc_port: int = 9000,
    ) -> None:
        self.openai_client = OpenAI(base_url=base_url, api_key="not-needed")
        self.context_prompt_file = Path(context_prompt_file)
        self.conversation_starter_file = (
            Path(conversation_starter_file) if conversation_starter_file else None
        )
        self.input_device_name = input_device_name
        self.output_device_name = output_device_name
        self.head_movement = head_movement
        self.osc_client = SimpleUDPClient("127.0.0.1", osc_port)
        self.config = configparser.ConfigParser(allow_no_value=True)
        self.gamepad = vg.VX360Gamepad()
        self.idle_timer = ResettableTimer(self.IDLE_DELAY, self.idle)
        self.inverted_move = False
        self.listener = None
        self._init_config()
        self._init_input_device()
        self._init_output_device()
        self._init_polly_client()
        self._init_user_replacements()
        self._init_idle_texts()
        self.randomize_movement()
        self.init_conversation_history()

    def _init_config(self) -> None:
        self._load_base_config()
        config_file = CONFIG_PATH / "config.ini"
        if config_file.exists():
            self.config.read(config_file, encoding="utf8")
        self.save_config()

    def _load_base_config(self) -> None:
        for section, options in self.CONFIG.items():
            self.config[section] = options

    def _init_input_device(self) -> None:
        input_devices = get_devices(device_type="input")
        self.input_device_index = None
        if self.input_device_name:
            for device in input_devices:
                if self.input_device_name in device["name"]:
                    self.input_device_index = device["index"]
                    break
            if not self.input_device_index:
                msg = f"No such device found '{self.input_device_name}'"
                raise ValueError(msg)

    def _init_output_device(self) -> None:
        output_devices = get_devices(device_type="output")
        self.output_device = None
        if self.output_device_name:
            for device in output_devices:
                if self.output_device_name in device["name"]:
                    self.output_device = device
                    break
            if not self.output_device:
                msg = f"No such device found '{self.output_device_name}'"
                raise ValueError(msg)

    def _init_polly_client(self) -> None:
        my_config = Config(region_name=self.config["API"]["aws_region"])
        self.polly_client = boto3.client(
            "polly",
            aws_access_key_id=self.config["API"]["aws_access_key"],
            aws_secret_access_key=self.config["API"]["aws_secret_key"],
            config=my_config,
        )

    def _init_user_replacements(self) -> None:
        default_user_replacements_file = CURRENT_DIR / "resources" / "user_replacements.yaml"
        user_replacements_file = CONFIG_PATH / "user_replacements.yaml"
        if not user_replacements_file.exists():
            shutil.copy(default_user_replacements_file, user_replacements_file)
        with user_replacements_file.open(encoding="utf8") as f:
            self.user_replacements = yaml.safe_load(f)

    def _init_idle_texts(self) -> None:
        default_idle_texts_file = CURRENT_DIR / "resources" / "idle_texts.yaml"
        idle_texts_file = CONFIG_PATH / "idle_texts.yaml"
        if not idle_texts_file.exists():
            shutil.copy(default_idle_texts_file, idle_texts_file)
        with idle_texts_file.open(encoding="utf8") as f:
            self.idle_texts = yaml.safe_load(f)

    def init_conversation_history(self) -> None:
        """Initialize or reset LolaBeta conversation history."""
        conversation_history = []
        system_prompt = self.config["PROMPT"]["system"]
        conversation_history.append({"role": "system", "content": system_prompt})
        context_prompt = self.context_prompt_file.read_text(encoding="utf8")
        conversation_history.append({"role": "user", "content": context_prompt})
        default_conversation_starter_file = CURRENT_DIR / "resources" / "conversation_starter.yaml"
        if not self.conversation_starter_file:
            self.conversation_starter_file = CONFIG_PATH / "conversation_starter.yaml"
            if not self.conversation_starter_file.exists():
                shutil.copy(default_conversation_starter_file, self.conversation_starter_file)
        with self.conversation_starter_file.open(encoding="utf8") as f:
            conversation_stater = yaml.safe_load(f)
            conversation_history = list(conversation_stater)
        self.conversation_history_base_length = len(conversation_history)
        self.conversation_history = conversation_history

    def manual_command(self, text: str) -> bool:
        """Execute a manual input command from the console.

        - head: will reset the head movement of LolaBeta.
        - reset: will reset the conversation history.
        - idle: toggle idle timer.
        - test: test your connection to the local language model.

        Other input text will speech manually your input text
        (and add your input text to the conversation history as it was LolaBeta talking).

        Parameters
        ----------
        text : str
            The command text.

        Returns
        -------
        bool
            True if a recognized command was executed successfully, False otherwise.
        """
        self.idle_timer.reset()
        if text.lower() == "head":
            self.reset_head_position()
            return True
        if text.lower() == "reset":
            self.reset_conversation()
            return True
        if text.lower() == "idle":
            self.toggle_idle()
            return True
        if text.lower() == "test":
            self.conversation_history.append({"role": "user", "content": "test"})
            self.answer_from_local_llm()
            return True
        self.speech(text)
        return False

    def start_listener(self) -> None:
        """Start the listener thread."""
        recognizer = speech_recognition.Recognizer()
        microphone = speech_recognition.Microphone(device_index=self.input_device_index)
        recognizer.pause_threshold = 0.5
        recognizer.non_speaking_duration = 0.5
        recognizer.dynamic_energy_threshold = False
        self.listener = recognizer.listen_in_background(microphone, self.recognize)
        logger.info("Ready")
        try:
            while True:
                self.manual_command(input("Command:"))
        except KeyboardInterrupt:
            logger.info("Goodbye!")
            self.idle_timer.cancel()
            sys.exit()

    def recognize(
        self,
        recognizer: speech_recognition.Recognizer,
        audio: speech_recognition.Microphone,
    ) -> None:
        """Use Google's Speech Recognition API to recognize an audio file into a text string.

        Then
        - Add the recognized text to the conversation history.
        - Speech the answer.
        - Detect repetitive responses (number of occurrences can be edited through configuration).
        - Perform head movement (if enabled).

        Note
        ----
        This method is intended to be used as an argument for the
        speech_recognition.Recognizer().listen_in_background function.

        Parameters
        ----------
        recognizer : `speech_recognition.Recognizer`
            A `Recognizer` instance from `speech_recognition` module used to perform speech recognition.
        audio : `speech_recognition.Microphone`
            A `Microphone` instance from `speech_recognition` module as the source of audio input.
        """
        self.idle_timer.reset()  # Every time someone interacts with the chatbot, the idle timer is reset
        self.send_osc_parameter("/chatbox/typing", value=True)  # Show that the chatbot is "thinking"
        try:  # Try to recognize audio into text
            text = recognizer.recognize_google(audio, language="en-EN")
            text = alternate(text, self.user_replacements)  # Correct recognizer mistakes
            logger.info("Recognized: %s", text)
            self.conversation_history.append({"role": "user", "content": text})
        except speech_recognition.UnknownValueError:  # Recognizer did not understand
            logger.warning("The voice recognition did not understand.")
            self.play_random_poi()
        except speech_recognition.RequestError:
            logger.exception("Could not request results from Google Speech Recognition service")
            write_chatbox(self.osc_client, "*I am confused*", chunk_length=0)
            # Lower / upper the ears
            self.send_osc_parameter("/avatar/parameters/Ears", value=False, sleep_time=5, end_value=True)
        else:
            if not (answer := self.answer_from_local_llm()):
                return
            self.speech(answer)  # Speech the answer
            self.detect_repetitive_responses()
        finally:
            self.send_osc_parameter("/chatbox/typing", value=False)  # Show that the chatbot is "Ready"
            if self.head_movement:
                self.perform_head_movement()

    def play_random_poi(self) -> None:
        """Play a random poi sound from the poi resources file.

        It is currently used when the voice recognition did not understand.
        """
        random_poi_sound = random.choice(POI_SOUNDS)
        self.send_osc_parameter("/avatar/parameters/Osc", 1, 0.1, 0)
        read_audio_file(str(random_poi_sound), self.output_device["name"])  # Read the audio file
        write_chatbox(self.osc_client, "ポイ")

    def answer_from_local_llm(self) -> str | None:
        """Get an answer from the Local Language Model (LLM).

        Usually I use LM-Studio.

        Returns
        -------
        str
            The generated answer by the model if any, otherwise returns None.
        """
        self.send_osc_parameter("/chatbox/typing", value=True)  # Show that the chatbot is "thinking"
        try:
            completion = self.openai_client.chat.completions.create(
                model="local-model",  # This field is currently unused
                messages=self.conversation_history,
                temperature=0.7,
            )
        except APIConnectionError:
            logger.exception("Connection error. Have you started LMStudio?")
            return None
        self.send_osc_parameter("/chatbox/typing", value=False)  # Show that the chatbot is "Ready"
        if answer := completion.choices[0].message.content.strip():
            logger.info("Answer from Local LLM: %s", answer)
            return answer
        return None

    def speech(self, text: str) -> None:
        """Convert a text into a speech and then play it to the self.output_device value.

        Also write the text to the Chatbox, and add the answer to the conversation history.

        Parameters
        ----------
        text : str
            The text to be converted to speech.
        """

        def convert_to_ssml(input_text) -> str:  # This defines the voice fine tunings
            return (
                '<speak><amazon:auto-breaths frequency="low" volume="soft" duration="x-short">'
                '<prosody pitch="high">' + input_text + "</prosody></amazon:auto-breaths></speak>"
            )

        def process_asterisks(input_text) -> str:  # This remove text between "*"
            return "".join(input_text.split("*")[0] + input_text.rsplit("*", 1)[-1])

        self.send_osc_parameter("/avatar/parameters/Osc", 1, 0.1, 0)
        processed_text = process_asterisks(text) if "*" in text else text
        filename = hashlib.sha256(processed_text.encode()).hexdigest()  # Hash the text for file name
        filepath = CONFIG_PATH / "voices" / "Ivy" / f"{filename}.ogg"
        filepath.parent.mkdir(parents=True, exist_ok=True)
        if not filepath.exists():  # Create the answer from Amazon Polly if the file does not exist
            self.send_osc_parameter("/chatbox/typing", value=True)  # Show that the chatbot is "thinking"
            response = None
            try:
                response = self.polly_client.synthesize_speech(
                    Text=convert_to_ssml(processed_text),
                    VoiceId="Ivy",
                    OutputFormat="ogg_vorbis",
                    TextType="ssml",
                )
                with filepath.open("wb") as file:
                    file.write(response["AudioStream"].read())
            except botocore.exceptions.ClientError:
                logger.exception()
            self.send_osc_parameter("/chatbox/typing", value=False)  # Show that the chatbot is "Ready"
            if not response:
                return
        read_audio_file(str(filepath), self.output_device["name"])  # Read the audio file
        write_chatbox(self.osc_client, text, font="Magic")  # Write the text to the Chatbox
        self.conversation_history.append({"role": "assistant", "content": text})  # update conv history

    def detect_repetitive_responses(self) -> None:
        """Check for repetitive responses from LolaBeta in the conversation history.

        If it finds any more than the configured occurence of them, then it resets the conversation.
        This is to prevent the model generating too many similar responses.

        Number of occurrences can be edited through configuration.
        """
        assistant_responses = [  # Get all assistant responses from conversation history
            item["content"] for item in self.conversation_history if item["role"] == "assistant"
        ]
        counter = Counter(assistant_responses)  # Calculate frequency of each response
        repetitive_occurrence = int(self.config["APP"]["repetitive_occurrence"])
        if repeaters := {k: v for (k, v) in counter.items() if v > repetitive_occurrence}:
            logger.debug("Repeater found '%s', resetting conversation...", repeaters)
            self.reset_conversation()

    def idle(self) -> None:
        """Generate a text idle after a certain time if no contact is established with the chatbot.

        If a conversation history exists, it is reset. Head movements are also reset.

        The idle texts can be configured in your %APPDATA%/LolaBeta/idle_texts.yaml
        """
        self.reset_conversation()
        idle_text = random.choice(self.idle_texts)  # Pick a random idle text from settings
        logger.debug("Idle text: %s", idle_text)
        write_chatbox(self.osc_client, idle_text, chunk_length=0)  # Write the text to the Chatbox
        self.reset_head_position()
        self.idle_timer.reset()

    def toggle_idle(self) -> None:
        """Toggle idle timer on or off based on a text message."""
        try:
            self.idle_timer.start()
            logger.debug("idle enabled.")
        except RuntimeError:
            self.idle_timer.cancel()
            logger.debug("idle disabled.")

    def send_osc_parameter(self, param: str, value, sleep_time: float | None = None, end_value=None) -> None:
        """Send an OSC message to VRChat with an optional sleep time and end value.

        Parameters
        ----------
        param : str
            The name of the OSC parameter.
        value
            The value to send to the OSC parameter.
        sleep_time : float
            How long to wait before sending the end_value.
        end_value
            The OSC value to send after the sleep_time has elapsed.
        """
        self.osc_client.send_message(param, value)
        if sleep_time and end_value is not None:
            sleep(sleep_time)
            self.osc_client.send_message(param, end_value)

    def perform_head_movement(self) -> None:
        """Perform head movement using gamepad."""
        self.gamepad.press_button(button=vg.XUSB_BUTTON.XUSB_GAMEPAD_X)  # Wake up the gamepad
        sleep(0.1)
        self.gamepad.release_button(button=vg.XUSB_BUTTON.XUSB_GAMEPAD_X)
        sleep(0.1)
        self.gamepad.update()
        if not self.inverted_move:  # Move head using random gamepad x and y values
            self.gamepad.right_joystick_float(x_value_float=self.move_x, y_value_float=self.move_y)
            self.gamepad.update()
            sleep(self.move_delay)
        else:  # Invert and move head using negative gamepad x and y values
            self.gamepad.right_joystick_float(x_value_float=-self.move_x, y_value_float=-self.move_y)
            self.gamepad.update()
            sleep(self.move_delay)
            self.randomize_movement()  # Randomize movement for future idle.
        self.gamepad.reset()
        self.gamepad.update()
        self.inverted_move = not self.inverted_move

    def randomize_movement(self) -> None:
        """Randomizes the head movement.

        It randomly selects a new x and y head position, along with a random delay,
        and sets these as the attributes for move_x, move_y, and move_delay respectively.
        These attributes are used to update the head movements in future idle.
        """
        self.move_x = random.choice([0.25, -0.25])
        self.move_y = random.choice([0.25, -0.25])
        self.move_delay = random.uniform(0, 1.0)

    def reset_head_position(self) -> None:
        """If the head is moved, reset to default position."""
        if self.inverted_move:
            logger.debug("Reseting head position in 5 seconds. Please click on the VRChat window.")
            sleep(5)
            self.perform_head_movement()
            logger.debug("Head position reseted.")
            return
        logger.debug("Reset head position not needed.")

    def reset_conversation(self) -> None:
        """Reset the conversation history."""
        if len(self.conversation_history) != self.conversation_history_base_length:
            self.init_conversation_history()
            logger.debug("Conversation reseted.")
            return
        logger.debug("Conversation reset not needed.")

    def save_config(self) -> None:
        """Save configuration changes to the configuration file."""
        config_file = CONFIG_PATH / "config.ini"
        config_file.parent.mkdir(parents=True, exist_ok=True)
        with config_file.open("w", encoding="utf-8") as f:
            self.config.write(f)


def alternate(text: str, replacement_dict: dict) -> str:
    """Alternate all instances of words in the text with their corresponding values in a dictionary.

    Note
    ----
    Replacements can be configured in your %APPDATA%/LolaBeta/user_replacements.yaml

    Parameters
    ----------
    text: str
        The text to be altered.
    replacement_dict: dict
        A dictionary of words to replace.

    Returns
    -------
        The text with the words replaced.
    """
    for word in text.translate(str.maketrans("", "", string.punctuation)).split(" "):
        if word.lower() in replacement_dict:
            text = re.sub(rf"\b{word}\b", replacement_dict[word.lower()], text)
            logger.debug("Altered string: %s > %s", word, replacement_dict[word.lower()])
    return text
