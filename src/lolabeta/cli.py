"""Console script for LolaBeta."""

import argparse

from lolabeta import LolaBeta


def main_cli() -> None:
    """Console script for LolaBeta."""
    parser = argparse.ArgumentParser(description="Run LolaBeta Chatbot.")
    parser.add_argument(
        "-u",
        "--url",
        type=str,
        default="http://localhost:1234/v1",
        help="The base URL of the Inference Server (default is 'http://localhost:1234/v1').",
    )
    parser.add_argument(
        "-cp",
        "--context-prompt",
        required=True,
        type=str,
        help="Path to the context prompt file. This file should contain the context prompt for LolaBeta, "
        "where you can provide details on her behavior or what she needs to learn... "
        "The file must be a .txt format.",
    )
    parser.add_argument(
        "-cs",
        "--conversation-stater",
        type=str,
        help="Path to a custom conversation starter file. This file represents the beginning of "
        "the conversation between LolaBeta and the user, providing more context for LolaBeta on "
        "how she should respond and behave. The rest of the conversation is then filled in by the "
        "application when it is running.",
    )
    parser.add_argument(
        "-i",
        "--input-device-name",
        type=str,
        help="Name of the input device you want to use for LolaBeta to listen.",
    )
    parser.add_argument(
        "-o",
        "--output-device-name",
        type=str,
        help="Name of the output device you want for LolaBeta to use for text to speech synthesis.",
    )
    parser.add_argument(
        "--head-movement",
        default=True,
        action=argparse.BooleanOptionalAction,
        help="Enable or disable head movement when LolaBeta respond to a user. True by default.",
    )
    parser.add_argument(
        "-p",
        "--osc-port",
        type=int,
        default=9000,
        help="Port number for VRChat OSC (default is 9000).",
    )
    parser.add_argument(
        "--idle",
        default=True,
        action=argparse.BooleanOptionalAction,
        help="Create an Idle timer for LolaBeta to display predefined texts in the Chatbox "
        "when no activity is detected.",
    )
    args = parser.parse_args()
    lola = LolaBeta(
        args.url,
        args.context_prompt,
        args.conversation_stater,
        args.input_device_name,
        args.output_device_name,
        args.head_movement,
        args.osc_port,
    )
    if args.idle:
        lola.idle_timer.start()
    lola.start_listener()
