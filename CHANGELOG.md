## [3.0.0](https://gitlab.com/ameliend/LolaBeta/compare/v2.1.0...v3.0.0) (2024-12-22)

### ⏩ Performance

* ruff linting ([3216ed4](https://gitlab.com/ameliend/LolaBeta/commit/3216ed4e680cf269fa68f9765cb459725c4f5e1d))

### 📔 Docs

* update README ([8b4c6ba](https://gitlab.com/ameliend/LolaBeta/commit/8b4c6ba4f4e4e69a5d586966a9d4624acb212778))
* update sphinx docs ([f8463f3](https://gitlab.com/ameliend/LolaBeta/commit/f8463f3cad1fbd57c5424a04791181692299903b))

### 🦊 CI/CD

* added uv and replaced pylint with ruff ([8874dd4](https://gitlab.com/ameliend/LolaBeta/commit/8874dd4ca46c3df816b41f148f56ac63d075f5a8))
* update mr templates ([6c9f297](https://gitlab.com/ameliend/LolaBeta/commit/6c9f297dd010455acbcf2d1191bee92809ba223c))

### 🧪 Tests

* update tests ([54419b0](https://gitlab.com/ameliend/LolaBeta/commit/54419b0c270a4837ef7f0946be9c071626bef019))

## [2.1.0](https://gitlab.com/ameliend/LolaBeta/compare/v2.0.2...v2.1.0) (2024-09-21)

### :scissors: Refactor

* upgrade to pyproject.tom workflow ([89c1c81](https://gitlab.com/ameliend/LolaBeta/commit/89c1c8124efb6ef58528686821e0f23f4e0e517b))

### 📔 Docs

* update logo ([334bc26](https://gitlab.com/ameliend/LolaBeta/commit/334bc2663e43465fe1fdc2b368122c0889d06dac))
* updated sphinx documentation ([5998938](https://gitlab.com/ameliend/LolaBeta/commit/599893820a93033c402979d0a5269a1e1ec4484a))

### 🦊 CI/CD

* update MANIFEST ([61172de](https://gitlab.com/ameliend/LolaBeta/commit/61172de7b5e52f09cb91725ece8917358717a060))

### 🚀 Features

* **logging:** added debug on reset head position not needed ([78ace87](https://gitlab.com/ameliend/LolaBeta/commit/78ace8708fe7158d767c0c5aff70d4da297525a6))
* **recognizer:** do a poi sound when the recognizer don't understand ([0babcf9](https://gitlab.com/ameliend/LolaBeta/commit/0babcf9086d0cbd0de7887656ab73947fb567608))

### 🛠 Fixes

* **config:** update repetitive occurrence threshold to 2 ([2a85e82](https://gitlab.com/ameliend/LolaBeta/commit/2a85e825f9862925b5d20280f423f059ced6b3ea))
* **config:** update the system prompt ([c732de3](https://gitlab.com/ameliend/LolaBeta/commit/c732de3deff495b134e491f48193f4ad7ecc204a))
* **speech:** update the osc avatar parameter address ([feaa4f9](https://gitlab.com/ameliend/LolaBeta/commit/feaa4f9e0956731d2efba92dbaad8bf6a3e146aa))

## [2.0.2](https://gitlab.com/ameliend/LolaBeta/compare/v2.0.1...v2.0.2) (2024-05-03)


### 👀 Reverts

* fix CURRENT_DIR changed previously with pathlib ([05494c8](https://gitlab.com/ameliend/LolaBeta/commit/05494c813f551455c38e3373e17c6ca10500f17c))


### 🦊 CI/CD

* fix CURRENT_DIR ([3ff974e](https://gitlab.com/ameliend/LolaBeta/commit/3ff974ebc40690d55be0591af117a36faf46b3b6))

## [2.0.1](https://gitlab.com/ameliend/LolaBeta/compare/v2.0.0...v2.0.1) (2024-05-03)


### :scissors: Refactor

* replaced SCRIPT_PATH to CURRENT_DIR ([6c94f8b](https://gitlab.com/ameliend/LolaBeta/commit/6c94f8bcc2bfd3edda45ece0d45cbbf0499e9426))


### 📔 Docs

* update docstrings ([3539370](https://gitlab.com/ameliend/LolaBeta/commit/3539370306187e5c6faf674eaf64c78a9119ccfe))
* update logo ([628d138](https://gitlab.com/ameliend/LolaBeta/commit/628d1384d7c6abc9286a93f8a3eeae183c7d1b8a))
* update README ([9f4382b](https://gitlab.com/ameliend/LolaBeta/commit/9f4382b1ce8b083b5746f070e0c1af8bf0e4d904))
* update Sphinx doc ([61ead24](https://gitlab.com/ameliend/LolaBeta/commit/61ead240cb2620b552b956c42bcf47cae7414de6))


### 🦊 CI/CD

* replaced SCRIPT_PATH to CURRENT_DIR ([230681b](https://gitlab.com/ameliend/LolaBeta/commit/230681b40f51ec75b57a9bd4d5cb9f46838ab8c1))

## [2.0.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.7.0...v2.0.0) (2024-04-28)


### :scissors: Refactor

* remove unused import ([697e73c](https://gitlab.com/ameliend/LolaBeta/commit/697e73c85223f1cdeb1c0dd38a0061a01ea9641c))
* **manual_command:** return False when speech ([15a5109](https://gitlab.com/ameliend/LolaBeta/commit/15a510956d917a2adb63418f5fb6e0740ee32e62))


### 📔 Docs

* update docstrings ([91c4144](https://gitlab.com/ameliend/LolaBeta/commit/91c41442828312d58d52185e6561c689f4ce349e))
* update LICENSE ([c4ea116](https://gitlab.com/ameliend/LolaBeta/commit/c4ea116f3c6427574c07a3fcd2b9845dda366159))
* update README ([98a7b20](https://gitlab.com/ameliend/LolaBeta/commit/98a7b205903c434234ab3fab1dcd08d77ad764e6))
* update sphinx doc ([c492d4d](https://gitlab.com/ameliend/LolaBeta/commit/c492d4db483b42db6d1693d5e3be1f63051d2694))


### 🦊 CI/CD

* update MANIFEST ([28495f4](https://gitlab.com/ameliend/LolaBeta/commit/28495f461d9cda93dbd668f4c62bea741f7cff55))
* downgrade to node 21 ([122b28a](https://gitlab.com/ameliend/LolaBeta/commit/122b28a1703ac88dec8c1cb3330de10660721748))
* update .releaserc.yml ([53170e9](https://gitlab.com/ameliend/LolaBeta/commit/53170e9145df5cae6c4ab08b84cd50a65d1881c9))
* update to python 3.11 ([3a61036](https://gitlab.com/ameliend/LolaBeta/commit/3a61036d92f3284882c1e6a42044fb6e75e569c8))


### 🧪 Tests

* update tests ([32f1438](https://gitlab.com/ameliend/LolaBeta/commit/32f14385cec8da8ae6e8952dc4ae1a287621f722))


### 🚀 Features

* added __author__ and __email__ to the package ([8d2458b](https://gitlab.com/ameliend/LolaBeta/commit/8d2458b4ca3825ade7b83008fe06385bb3aa6eb9))
* added .editorconfig ([4b32263](https://gitlab.com/ameliend/LolaBeta/commit/4b322631bcdc09745a473d92d387d2a44d6cdb33))

## [1.7.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.6.1...v1.7.0) (2024-2-4)


### 📔 Docs

* update README ([7774190](https://gitlab.com/ameliend/LolaBeta/commit/77741906ace6a1b2b1519559903d6497f82cb347))
* update sphinx documentation ([709b2f2](https://gitlab.com/ameliend/LolaBeta/commit/709b2f2a1a71d5329b0b47810d7490144eba2bc3))


### 🦊 CI/CD

* **manifest:** add prompt file to be included in package distribution ([63b0d88](https://gitlab.com/ameliend/LolaBeta/commit/63b0d88a979ac0bafd3a6916d7a8712f0e1d5d66))
* **requirements:** removed cleverbot and add openai as a new dependency ([9216045](https://gitlab.com/ameliend/LolaBeta/commit/9216045a5b29d318d01427003b021aa37f4483ce))


### 🧪 Tests

* update tests ([aaa7a96](https://gitlab.com/ameliend/LolaBeta/commit/aaa7a965822faad0d6ad2c53dc3f540a816bae60))


### 🚀 Features

* cleverbot replaced by local LLM ([7081b42](https://gitlab.com/ameliend/LolaBeta/commit/7081b42f10324dd6e009ed567c85c90eb7b81151))

## [1.6.1](https://gitlab.com/ameliend/LolaBeta/compare/v1.6.0...v1.6.1) (2024-1-13)


### 📔 Docs

* update docs ([97e9c3c](https://gitlab.com/ameliend/LolaBeta/commit/97e9c3c752bc59b5b1becf406c915457e8bec1d0))


### 🛠 Fixes

* **settings:** correct spelling of "traveler" in replacements and reactions ([d611e63](https://gitlab.com/ameliend/LolaBeta/commit/d611e63ae22cc6f464f081579b214c44e2ae0856))

## [1.6.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.5.0...v1.6.0) (2024-1-7)


### :scissors: Refactor

* **CustomReaction:** removed custom reaction for "the void" ([e8a9e47](https://gitlab.com/ameliend/LolaBeta/commit/e8a9e47d214888256aeed1479cc9be27503f8878))


### 💈 Style

* wrap long string literals ([b9cf5de](https://gitlab.com/ameliend/LolaBeta/commit/b9cf5de65e59cdd2239b26c2d2d254df2f9412ed))
* **settings:** reformat using black ([d4137e0](https://gitlab.com/ameliend/LolaBeta/commit/d4137e08d857fdc14f76d88c50e04511c80af61f))


### 📔 Docs

* build doc ([46f885f](https://gitlab.com/ameliend/LolaBeta/commit/46f885f5ff845d87f9e014fef4afca97ae3a5847))
* update documentation links ([4a70f42](https://gitlab.com/ameliend/LolaBeta/commit/4a70f42f7246e70f20e9ae8e4cf6ae61a9b6f48a))
* update pages ([4024751](https://gitlab.com/ameliend/LolaBeta/commit/4024751deb7c772aca40b960d832f18e5a452b64))
* **README:** update README with new features and API docs link ([ce226c0](https://gitlab.com/ameliend/LolaBeta/commit/ce226c0b55c765299293d0de2170a0cc9a3ff2f7))
* **write_chatbox:** update docstrings ([fc3b464](https://gitlab.com/ameliend/LolaBeta/commit/fc3b464350e354e3185d9c4b224197d8f7063915))


### 🦊 CI/CD

* **setup:** ensure README file read with utf-8 encoding ([9af635c](https://gitlab.com/ameliend/LolaBeta/commit/9af635c05b1551c9e966943ca9c261888abd5be7))
* add artifact dependency to pages job in GitLab CI configuration ([644f457](https://gitlab.com/ameliend/LolaBeta/commit/644f4579c50fa2b47d3fdbd36d31871e8ffdc862))
* add sphinx-automodapi job to GitLab CI for documentation stage ([59163c8](https://gitlab.com/ameliend/LolaBeta/commit/59163c8042b05efa0a8926e858eeaea691e90218))
* remove sphinx-automodapi job from GitLab CI configuration ([03d15cf](https://gitlab.com/ameliend/LolaBeta/commit/03d15cfacfc975bc4cc20bb8c4eca01ee2867b72))


### 🚀 Features

* **CustomReaction:** expand LolaBeta dialogs for richness using AI ([61af871](https://gitlab.com/ameliend/LolaBeta/commit/61af8715b3f513703c4c942791b8d48bb7f9aadc))
* **CustomReaction:** update VRChat world suggestions ([26f6975](https://gitlab.com/ameliend/LolaBeta/commit/26f697545ab90b87044660718a1a51076d53b908))


### 🛠 Fixes

* **recognize:** added back logging for recognized text ([c814ebb](https://gitlab.com/ameliend/LolaBeta/commit/c814ebb1cab10be3a370d6e8e0508d10507c4811))

## [1.5.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.4.3...v1.5.0) (2024-1-1)


### 📔 Docs

* update copyright year to 2024 ([ddbd8e9](https://gitlab.com/ameliend/LolaBeta/commit/ddbd8e95b3a30337cab82f3a83a6fdfd2e628973))
* **automodapi:** include automodapi documentation generation to Gitlab Pages ([5f58268](https://gitlab.com/ameliend/LolaBeta/commit/5f58268e6211edd5ed8fc552af56d10cf0ef17e0))
* **README:** include coverage ([21c3102](https://gitlab.com/ameliend/LolaBeta/commit/21c3102588ef2f0c2668e9bbcc3b5345bf8a3614))


### 🦊 CI/CD

* update author email ([8268ac1](https://gitlab.com/ameliend/LolaBeta/commit/8268ac13c805ac47a12a7debb06ecaac2954ad5e))
* update Python compatibility, add classifiers and documentation link ([f99f50c](https://gitlab.com/ameliend/LolaBeta/commit/f99f50c94ca36bf26873a0067180b86baae1d73c))
* update pipeline ([1e2b92e](https://gitlab.com/ameliend/LolaBeta/commit/1e2b92ee227453eb35e49ef05c7b4094459f1383))
* **merge_request_templates:** update merge request checklist to generalize Pylint requirement ([f064108](https://gitlab.com/ameliend/LolaBeta/commit/f0641089cfa419033bc985d0ec9051fe50706f1e))


### 🧪 Tests

* add tests ([815d574](https://gitlab.com/ameliend/LolaBeta/commit/815d5747f0e1e6c4508545fef614f273c39392b8))


### 🚀 Features

* upgrade speech output handling and chatbox ([a6a6aca](https://gitlab.com/ameliend/LolaBeta/commit/a6a6aca92ede071b80f2789109b497db80e43fd7))

## [1.4.3](https://gitlab.com/ameliend/LolaBeta/compare/v1.4.2...v1.4.3) (2023-08-04)


### :scissors: Refactor

* update logger ([78d4182](https://gitlab.com/ameliend/LolaBeta/commit/78d4182eef88e2cbac123b321c79154a4eebe9b2))


### 🦊 CI/CD

* update setup.py ([ff27f27](https://gitlab.com/ameliend/LolaBeta/commit/ff27f272b005ff8b8f313db87c047c8bec3508a2))

## [1.4.2](https://gitlab.com/ameliend/LolaBeta/compare/v1.4.1...v1.4.2) (2023-08-01)


### 🛠 Fixes

* missing requirements ([2ad2341](https://gitlab.com/ameliend/LolaBeta/commit/2ad23415e09700101761fe19695610661ff0229a))

## [1.4.1](https://gitlab.com/ameliend/LolaBeta/compare/v1.4.0...v1.4.1) (2023-07-24)


### ⏩ Performance

* removed dynamic energy threshold ([7d21efe](https://gitlab.com/ameliend/LolaBeta/commit/7d21efe2a4014511ff716bf1176043f2943d8ef5))

## [1.4.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.3.0...v1.4.0) (2023-06-21)


### :scissors: Refactor

* print more debug ([f222c8f](https://gitlab.com/ameliend/LolaBeta/commit/f222c8f4dfc1e10806f62f419df6dc2b52565874))


### ⏩ Performance

* optimized code ([67cf044](https://gitlab.com/ameliend/LolaBeta/commit/67cf04465a8e4f149536aaa9cce4273e17b43ae7))


### 📔 Docs

* udpate README ([4f09538](https://gitlab.com/ameliend/LolaBeta/commit/4f09538864e6a59e9cdb257de5a9e0a89ad133cf))
* update docstrings ([175c21c](https://gitlab.com/ameliend/LolaBeta/commit/175c21c81e54352d294b05d556bbff566b687f1f))
* update LICENSE ([49f639a](https://gitlab.com/ameliend/LolaBeta/commit/49f639ad7e136ab2e5510765d17c5d8d3c4ee5ab))


### 🦊 CI/CD

* update requirements version denpendencies ([937ee45](https://gitlab.com/ameliend/LolaBeta/commit/937ee4562ddb4a738e0863785a2ca821523a704b))
* update requirements versions ([2443386](https://gitlab.com/ameliend/LolaBeta/commit/244338699e9ef403d15dfd5ed4f46e72367df0b2))
* update gitlab merge request template ([5d3cfd6](https://gitlab.com/ameliend/LolaBeta/commit/5d3cfd6519737f10464c3e2d12c4f5124dfbb7bb))


### 🚀 Features

* add and delete some custom reactions ([cd2a844](https://gitlab.com/ameliend/LolaBeta/commit/cd2a8446b7fcd4843276165861d27d4192eabc40))
* add cleverbot custom reaction ([3441771](https://gitlab.com/ameliend/LolaBeta/commit/34417711cea1422d5d3e0e280cbfec5f5e6d5935))
* reset cleverbot conversation when idle ([77052fb](https://gitlab.com/ameliend/LolaBeta/commit/77052fb9168dfd594d0da5c277aa8da1fb5aa2ac))
* reset convertation on idle ([9dffdd5](https://gitlab.com/ameliend/LolaBeta/commit/9dffdd5ff927fb081ae1da95d9ff9a177a4a87b7))


### 🛠 Fixes

* manual_command ([c1cc2dd](https://gitlab.com/ameliend/LolaBeta/commit/c1cc2dd572a887d08144425d00d9799cf03bf2df))
* toggle idle ([45c3e02](https://gitlab.com/ameliend/LolaBeta/commit/45c3e027a8369ae00976e65a212cd390c53ea794))
* toggle_idle ([f0b513a](https://gitlab.com/ameliend/LolaBeta/commit/f0b513a473abb426c8c771a51a6f81b68730c8d9))
* **toggle_idle:** Return correct bool status ([5ebfe36](https://gitlab.com/ameliend/LolaBeta/commit/5ebfe367b027c9c7f4ee34e2afa477281ec2ffb6))


### Other

* removed python2 coding arguments ([63e2604](https://gitlab.com/ameliend/LolaBeta/commit/63e2604c64a598b774117f1f6aa9f3002e645729))
* update pylint config ([7e4a995](https://gitlab.com/ameliend/LolaBeta/commit/7e4a995307991979bd1a5014b8bbd63e6826513d))

## [1.3.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.2.1...v1.3.0) (2023-05-05)


### :scissors: Refactor

* reformat code ([6cac774](https://gitlab.com/ameliend/LolaBeta/commit/6cac77453392316dacefc5f43ef7c27efc38b031))


### ⏩ Performance

* custom reactions now use NamedTuple ([9819a12](https://gitlab.com/ameliend/LolaBeta/commit/9819a12151b59a5b4f3006ff07cb8e402f814ad8))


### 📔 Docs

* update docstrings ([55afec4](https://gitlab.com/ameliend/LolaBeta/commit/55afec429949b9ce763768346c1249c6c29d483f))


### 🦊 CI/CD

* update requirements ([b9b2515](https://gitlab.com/ameliend/LolaBeta/commit/b9b251553e6c621d0d0b1441e97f73c81ff237ca))


### 🚀 Features

* Lola can move head using gamepad inputs ([7f672a5](https://gitlab.com/ameliend/LolaBeta/commit/7f672a5f5a86cd140ec5d61268bc9e92941d2921))
* lola will make ❔when she doesn't understand ([fb4bb3a](https://gitlab.com/ameliend/LolaBeta/commit/fb4bb3a087d6b8c37a18608d0b55170c49caf9b1))
* Lola will make some idle text ([5ab757e](https://gitlab.com/ameliend/LolaBeta/commit/5ab757e0efa3579ecc5fd6a202cdc0448a83489a))
* Lola will use some blendshapes when talking ([8638d83](https://gitlab.com/ameliend/LolaBeta/commit/8638d83cab44fced5fe8296497dba241748c3535))
* manual command can now swith language ([16313f9](https://gitlab.com/ameliend/LolaBeta/commit/16313f95eba987d1519e2bcf6a4d5004841763fc))

## [1.2.1](https://gitlab.com/ameliend/LolaBeta/compare/v1.2.0...v1.2.1) (2023-02-10)


### :scissors: Refactor

* update user replacement dict ([4a8b74e](https://gitlab.com/ameliend/LolaBeta/commit/4a8b74e5eb743ecefb9b9c098e0137745e0b44e0))


### 📔 Docs

* update docs ([4c2042e](https://gitlab.com/ameliend/LolaBeta/commit/4c2042eef4b43fb8f996dedd088c60b2d7a0dcd0))


### 🦊 CI/CD

* fix requirements ([345f7a7](https://gitlab.com/ameliend/LolaBeta/commit/345f7a7f9217a618f51d54ecc7d7e723a22beb9a))
* update pylint ([3a1fd0f](https://gitlab.com/ameliend/LolaBeta/commit/3a1fd0f94bd2a46a3ed37dd3bff1b18bc434d39a))

## [1.2.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.1.1...v1.2.0) (2023-01-09)


### ⏩ Performance

* made some method static, and extract other into functions. ([1bcd600](https://gitlab.com/ameliend/LolaBeta/commit/1bcd600ea833d5270d0c07988d488cfa8ea06d67))
* pylint ([96c4bb1](https://gitlab.com/ameliend/LolaBeta/commit/96c4bb195c665268966e29b955b449e361f57468))


### 💈 Style

* reformat ([c14ffd5](https://gitlab.com/ameliend/LolaBeta/commit/c14ffd55a7b5a6828fccd07b2bf8c8e43b2c3d02))


### 📔 Docs

* made README ([594fc37](https://gitlab.com/ameliend/LolaBeta/commit/594fc3726b83602c4963026409cc199f88c6269f))


### 🦊 CI/CD

* update semantic release ([dbdee4d](https://gitlab.com/ameliend/LolaBeta/commit/dbdee4daa3736f93a2048b3d1e2cfb048ee11193))


### 🧪 Tests

* Add more custom reactions. ([df99bf6](https://gitlab.com/ameliend/LolaBeta/commit/df99bf6bd20c7a80f3b609e71313e2298a34373a))
* changed recognizer threshold from 0.5 to 0.75 ([768ce7b](https://gitlab.com/ameliend/LolaBeta/commit/768ce7bdf64cd468c6746fbca1cb7420a5a70786))


### 🚀 Features

* add "Hoppou" replacement for "human being" ([1dd0bd7](https://gitlab.com/ameliend/LolaBeta/commit/1dd0bd7d2fea53e891ad00969d9aa7f4fab21a3f))
* added more replacements. ([7c44fac](https://gitlab.com/ameliend/LolaBeta/commit/7c44facbb40f48ac0dbde46b8b1337df48e41937))
* added tweak argument to cleverbot answer (tweak2 correspond to shyness) ([f81a702](https://gitlab.com/ameliend/LolaBeta/commit/f81a702230f1ee8bf2c7e18295db1556659543e9))
* change recognizer threshold from 0.75 to 0.5 ([e6fa786](https://gitlab.com/ameliend/LolaBeta/commit/e6fa7867183481c1154d4af102b14e38365845e7))
* cleverbot conversation is reseted when language change. ([35d66b5](https://gitlab.com/ameliend/LolaBeta/commit/35d66b5596b0513402c2a023967aba87189fc2ad))


### 🛠 Fixes

* add language sub-folder for voices storage. ([e01e224](https://gitlab.com/ameliend/LolaBeta/commit/e01e2245dcde9ded01b3cec930d0725e60c30651))
* change sleep duration on OSC_poi from 0.5 to 0.1 ([cef295d](https://gitlab.com/ameliend/LolaBeta/commit/cef295da64bbc137668117b52b5a79828d68c799))
* fix imports ([5248777](https://gitlab.com/ameliend/LolaBeta/commit/52487770a606a2facc1722b7108969efa1a78174))
* fix issue when end_value of send_osc_parameter was 0. ([f604741](https://gitlab.com/ameliend/LolaBeta/commit/f604741bc3904a305f91afe8c66fb9d7cc1941c0))
* Fix OSC_Poi parameter delay too short causing infinite loop ([24405d4](https://gitlab.com/ameliend/LolaBeta/commit/24405d49897587773037fea14409ea0d119d48d5))
* fix word been lowered when word is in replacement_dict. ([c575051](https://gitlab.com/ameliend/LolaBeta/commit/c575051b73ac44e6d2c2a651ee1a6fd1266b904c))
* prevent key error for alternate using lower(). ([fe47976](https://gitlab.com/ameliend/LolaBeta/commit/fe47976a94cb314f24ac4b08e7aa1af66eae7ef7))
* Removing "human being" key causing key error because of whitespace. ([75ff866](https://gitlab.com/ameliend/LolaBeta/commit/75ff86617c809d1d8b8c52970978becfa99b13c0))
* **alternate:** lower() all the word in text to ignore case. ([c37a7b8](https://gitlab.com/ameliend/LolaBeta/commit/c37a7b8dc2619c688f666da183f0a0454040d903))

## [1.1.1](https://gitlab.com/ameliend/LolaBeta/compare/v1.1.0...v1.1.1) (2022-11-11)


### 🛠 Fixes

* fix crash if no voice availible for the given language ([cbf2959](https://gitlab.com/ameliend/LolaBeta/commit/cbf29590b6c7c2ba9d83118523365f743bf94281))

## [1.1.0](https://gitlab.com/ameliend/LolaBeta/compare/v1.0.0...v1.1.0) (2022-11-11)


### :scissors: Refactor

* change delay for ears going down / up when not understanding ([e84d0ac](https://gitlab.com/ameliend/LolaBeta/commit/e84d0ac8f11092843746a1aafa4571449532b273))
* make custom_answer a staticmethod ([deebe7f](https://gitlab.com/ameliend/LolaBeta/commit/deebe7f6c6436120ede933a00f83f5fe5b16f04f))
* official cleverbot api ([17ef734](https://gitlab.com/ameliend/LolaBeta/commit/17ef734acba4159de5e2cd77d28dfc9bdfa0bc23))
* removed winsound ([bda18fc](https://gitlab.com/ameliend/LolaBeta/commit/bda18fc1a157d383bb1987b80d8888e87fad16ac))
* renamed package to LolaBeta ([d15f228](https://gitlab.com/ameliend/LolaBeta/commit/d15f228820d774f03c09f12f7071c87488903a2d))
* replaced cleverbot api ([179d206](https://gitlab.com/ameliend/LolaBeta/commit/179d2068da1526ac20450240fc6dc6505a45cdde))
* simplified dictionaries to keep only one voice per languages. ([41bd764](https://gitlab.com/ameliend/LolaBeta/commit/41bd7644627d99eeff8020c08aa637d3aa3289cd))


### ⏩ Performance

* don't create an AmazonPolly request if the file already exists. ([4e8af63](https://gitlab.com/ameliend/LolaBeta/commit/4e8af636f8041296022823d7e283e648189a5a64))
* isort ([c9d9445](https://gitlab.com/ameliend/LolaBeta/commit/c9d94457762e3725339b8415124445c2e3b5a242))
* pylint ([aa9ad8a](https://gitlab.com/ameliend/LolaBeta/commit/aa9ad8a629d2434d99b2004bad9f774d00acf04d))
* pylint ([e02666f](https://gitlab.com/ameliend/LolaBeta/commit/e02666f28011d8e346f26e08daa963e20600db34))
* pylint and docstrings ([669c80b](https://gitlab.com/ameliend/LolaBeta/commit/669c80b8e613a0c2305fb71e7c494cd1f9842d81))


### 📔 Docs

* add README ([5552ef2](https://gitlab.com/ameliend/LolaBeta/commit/5552ef2f4494d4906097444433e6fdbc95b31537))
* make some docstrings ([2fa8d3c](https://gitlab.com/ameliend/LolaBeta/commit/2fa8d3c8215f0735a374669c5c14bcb2fad4130d))
* update logo ([860186d](https://gitlab.com/ameliend/LolaBeta/commit/860186d3b577b8c2b0ef1027258efe984d649034))


### 🦊 CI/CD

* add Logger requirement ([3582774](https://gitlab.com/ameliend/LolaBeta/commit/35827749a0abd69f1fd2dba04aa618756db496bf))
* add requirements ([1539df3](https://gitlab.com/ameliend/LolaBeta/commit/1539df3aed002a7235ce8b385d2dc2ba31328c5f))
* updated pylintrc file ([57dc7eb](https://gitlab.com/ameliend/LolaBeta/commit/57dc7ebcaed12a65a58e7792e9b5a4d9ec80bf03))


### 🚀 Features

* add custom reactions ([cd1824b](https://gitlab.com/ameliend/LolaBeta/commit/cd1824b81baa75ca3a6a5c191b7174b58c588f37))
* add osc avatar parameter reactions ([eede55a](https://gitlab.com/ameliend/LolaBeta/commit/eede55a3fe2928cd7fe4058256779fdb335fbf27))
* add vrchat custom reaction ([36c5ef7](https://gitlab.com/ameliend/LolaBeta/commit/36c5ef7658532fe0b7bf461660a13fe1a162c827))
* added listener in background thread ([5c2b7f4](https://gitlab.com/ameliend/LolaBeta/commit/5c2b7f4f44a02b7b5a723d79396f1ca7ced502ae))
* added multiple languages support ([55aac85](https://gitlab.com/ameliend/LolaBeta/commit/55aac85c134bde686c8827056a7a735082b4236e))
* initial testing recognizer and cleverbot ([e8df157](https://gitlab.com/ameliend/LolaBeta/commit/e8df1576d9859b19d7a9bd3da486e4d004546852))
* speech ([8ba9a07](https://gitlab.com/ameliend/LolaBeta/commit/8ba9a07947585e4d4b720a5725f1392eb5451a07))
* vrchat chatbox ([e72ea3b](https://gitlab.com/ameliend/LolaBeta/commit/e72ea3b585a71ba74689cba8b0aa021f50780cf6))


### 🛠 Fixes

* add missing threading event and device index 7 for audio stream ([28afa56](https://gitlab.com/ameliend/LolaBeta/commit/28afa560d74f73092f1ae922a293fb2a2da4240d))
* reformat file and fix encoding answer from cleverbot. ([fd13910](https://gitlab.com/ameliend/LolaBeta/commit/fd13910d157f9123e0cf4010b3dddaa4e2160d88))
* wrong key for Dutch voice ([7dcec7d](https://gitlab.com/ameliend/LolaBeta/commit/7dcec7d040829c7d15d69ad9068122210e181ced))

## [1.0.0](https://gitlab.com/ameliend/kittysan-os/compare/...v1.0.0) (2022-11-08)


### 🦊 CI/CD

* remove docs pipeline ([3969f4f](https://gitlab.com/ameliend/kittysan-os/commit/3969f4ffe4468f332c6c97bfda437ae104ba6d7a))


### 🚀 Features

* initial commit ([587155d](https://gitlab.com/ameliend/kittysan-os/commit/587155d1efda71c138f67d412f9ff7d0a40c54b2))
